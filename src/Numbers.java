import java.math.BigInteger;

public class Numbers {
	public static void main(String[] args) {
		BigInteger res = BigInteger.ONE;
//		long res = 1;
		for (long i = 1; i <= 50; i++){
			if (i == 31 || i == 32){
				continue;
			}
			res = lcm(res, BigInteger.valueOf(i));
		}
		System.out.println(res);
		for (int i = 1; i <= 50; i++){
			BigInteger mod = res.mod(BigInteger.valueOf(i));
			if (mod.equals(BigInteger.ZERO)){
				System.out.println(res.toString() + " mod " + i + " = 0");
			} else {
				System.err.println(res.toString() + " mod " + i + " = " + mod.toString());
			}
		}
	}
	
	/**
	 * Greatest Common Divisor
	 * @param n
	 * @param m
	 * @return gcd of n & m
	 */
	public static long gcd(long n, long m){
		long res = n%m;
		if (res == 0){
			return m;
		} else {
			return gcd(m, res);
		}
	}
		
	/**
	 * Least Common Multiple
	 * @param n
	 * @param m
	 * @return lcm
	 */
	public static long lcm(long n, long m) {
		return n*m/gcd(n,m);
	}
	
	public static BigInteger lcm(BigInteger n, BigInteger m) {
		return n.multiply(m).divide(n.gcd(m));
	}
}
