
public class UnsafeSequence {

	private int value;
	
	private static final int COUNT = 10000000 * 8 * 2;
	
	private static final int TH_COUNT = 2;
	
	private static int[] values = new int[COUNT * TH_COUNT];
	
	private static int finished = 0;
	
	public int getNext(){
		return value++;
	}
	
	public static void main(String[] args) {
		final long start = System.currentTimeMillis();
		
		final UnsafeSequence sequence = new UnsafeSequence();
		
		for (int i = 0; i < TH_COUNT; i++){
			
			final int index = i;
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					for (int i = 0; i < COUNT; i++){
						int res = sequence.getNext();
						synchronized (sequence) {
							values[res]++;
//							System.out.println(index + ": " + res);
						}
					}
					synchronized (sequence) {
						finished++;
						sequence.notify();
					}
					System.out.println(index + ": end");
				}
			}).start();
		}
		
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					synchronized (sequence) {
						while (finished < TH_COUNT){
							sequence.wait();
						}
					}
					int countTwo = 0;
					int countOverhead = 0;
					int countZero = 0;
					for (int i = 0; i < COUNT * TH_COUNT; i++){
						if (values[i] > 1){
//							System.out.print(i + " ");
							countTwo++;
							countOverhead += values[i]-1;
							if (values[i] > 3){
								System.err.println("More then three: " + values[i]);
							}
						} else if (values[i] == 0){
							countZero++;
						}
					}
					System.out.println();
					System.out.println("countTwo " + countTwo);
					System.out.println("countOverhead " + countOverhead);
					System.out.println("countZero " + countZero);
					long total = sequence.getNext() + countOverhead; 
					if (total == COUNT * TH_COUNT){
						System.out.println("Total " + total);
					} else {
						System.err.println("Total " + total);
					}					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				long end = System.currentTimeMillis();
				System.out.println();
				System.out.println("TIME: " + (end - start));
			}
		}).start();
	}
}
